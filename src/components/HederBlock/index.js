import React from 'react';
import s from './HeaderBlock.module.scss';
import  ReactLogo from '../../logo.svg';
const HeaderBlock = () =>{
    return (
      <div className={s.cover}>
          <div  className={s.wrap}>
              <h1 className={s.header}>learn words</h1>
             <img src={ReactLogo}/>
              <p className={s.decr}> Some random description</p>
          </div>
      </div>
    );
};

export default HeaderBlock;
