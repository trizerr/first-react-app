import React from "react";
import ReactDom from 'react-dom';
import HeaderBlock from "./components/HederBlock";
const AppList =()=>{
    const items = ['item 1', 'item 2'];
    const firstItem = <li>{items[0]}</li>;
    return (
      <ul>
          <li>item 0</li>
          <li>item 1</li>
      </ul>
    );
};

const AppHeader = ()=><h1 className="header">My Header</h1>;

    const AppInput =()=>{
        const placeholder = 'type smth';
      return [
          <label htmlFor="search">
              <input id="search" placeholder={placeholder}/>
          </label>
      ]
    };
const App = () => {
    return (
        <>
            <HeaderBlock/>
            <AppHeader/>
            <AppInput/>
            <AppList/>
        </>
    );
};

    ReactDom.render(<App/>, document.getElementById('root'));
